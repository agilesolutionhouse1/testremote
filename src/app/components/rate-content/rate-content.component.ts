import { Component, Input, OnInit } from '@angular/core';
import { Content } from 'src/app/model/content.class';

@Component({
  selector: 'app-rate-content',
  templateUrl: './rate-content.component.html',
  styleUrls: ['./rate-content.component.scss']
})
export class RateContentComponent implements OnInit {

  constructor() {
    this.max = 10;
    this.min = 0;
   }
  public max: number;
  public min: number;

  @Input() content: Content;
  @Input() counter: number;
  
  ngOnInit(): void {
  }

  increment() { 
    if (this.content.rate < this.max) {
      this.content.rate++;
    }
  }

  decrement() {
    if (this.content.rate > this.min) {
      this.content.rate--;
    }
  }
}

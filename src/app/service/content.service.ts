
import { Injectable } from '@angular/core';
import { ContentTag } from '../model/content-tag.class';
import { Content } from '../model/content.class';

@Injectable()
export class ContentService {
  constructor() { }

  public getContent() : Content[] {
    const passage:string = 'Lorem ipsum dolar sit amet, consectetur adipiscing elit. Cras at orci id sem interdum auctor. Vivamus nisl purus, cursus' +
    'nec lorem sit amet, facilisis commodo orci. Cras vitae iaculis velit. In ullamcorper velit non metus tempor, nec bibendum' +
    'urna porta. Ut nec neque consequat, rhoncus sem ac, placerat est. Quisque vel lacinia eros, eu dignissim leo. Nullam' +
    'dapibus tellus in eleifend tincidunt. Proin ullamcorper, lectus sit amet dignissim consectetur, quam quam tristique metus,' +
    'eget faucibus velit justo in tellus. Curabitur pellentesque tincidunt ornare. Nam semper nbulla turpis, sit amet aligquam' +
    'magna rhoncus vitae, Sed fringilla augue eu ultricies convallis.  Pellentesque lacinia id nibh ac rhoncus. Vestibulum sapien' +
    'ligula, adipiscing et nisi id, feugiat tristique erat. Suspendisse none est metus.' + '                                     ' +
    'Quisque lacinia hendrerit elit, in eleifend velit accumsan nec. Donec egestas consectetur dignissim. Maecenas et blandit' +
    'neque.  Integer eu venenatis justo. Phasellus condimentum libero in lectus lacinia interdum.  Vestibulum et mauris non' +
    'magna vulpuitate viverra.  Vivamus eleifend, massa sed bibnendum fringilla, mi quam dictum ligula, ut laoreet dui tellus at' +
    'diam. Ut prottitor nisi lectus, vitae aliquam tellus sagittis eu.  Etiam vestibulum sum magna, pharetra auctor tortor vulputate.'

    const line2:string = 'two';
    const line3:string = 'three';
    const line4:string = 'four';

    const tags: ContentTag[] = [
      new ContentTag ('tag1', ''),
      new ContentTag ('tag2', ''),
      new ContentTag ('tag3', '')
    ];

    return [
      new Content(1, passage, 0, 'Heading One', tags),
      new Content(2, passage, 0, 'Heading Two', tags),
      new Content(3, passage, 0, 'Heading Three', tags),
      new Content(4, passage, 0, 'Heading Four', tags),
    ]
  }
}

export class ContentTag {
    constructor(public tagName: string, public link: string) {}
}
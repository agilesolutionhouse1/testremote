import { ContentTag } from "./content-tag.class";

export class Content {
    constructor(
    public id: number,
    public content: string,
    public rate: number,
    public title: string,
    public tags: ContentTag[]) {}
}
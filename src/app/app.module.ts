import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { AboutComponent } from './component/about/about.component';
import { PortfolioComponent } from './component/portfolio/portfolio.component';
import { BlogComponent } from './component/blog/blog.component';
import { RateContentComponent } from './components/rate-content/rate-content.component';
import { ContentService } from './service/content.service';
import { MenuComponent } from './components/menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    PortfolioComponent,
    BlogComponent,
    RateContentComponent,
    MenuComponent
  ],
  imports: [  
    BrowserModule,
    AppRoutingModule
  ],
  providers: [ ContentService ],
  bootstrap: [AppComponent]
})
export class AppModule { }

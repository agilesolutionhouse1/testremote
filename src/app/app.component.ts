
import { Component } from '@angular/core';
import { Content } from './model/content.class';
import { ContentService } from './service/content.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public contentItems: Content[];
  constructor(private contentService: ContentService) {
    this.contentItems =contentService.getContent();
  }
  title = 'remote-test';
}
